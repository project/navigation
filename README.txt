# Drupal core new administration navigation
This repo contains the initial work for the reworking of the core administration toolbar in Drupal core. We are building an HTML mockup of a left-aligned, collapsible, vertical sidebar navigation. This exploration/prototype serves a specific purpose: to test and check its feasibility.

The longer-term goal is to propose the new navigation for Drupal core as part of the plan Administration UX improvements (https://www.drupal.org/project/drupal/issues/3373303).

# Installation
Download a regular Drupal module and install it. It'll replace the existing Toolbar with the new Sidebar.

## Local setup for development
1. In your terminal, run `yarn install`
2. In your terminal, run `yarn run build` to compile CSS changes or `yarn run watch` to watch for changes.

## Coding standards
Use the new Coding Standards we're preparing for core: https://docs.google.com/document/d/1rihTDDGy9-m0TGIadgz16TQ1aONc0OC3u2TkJ8bNaxg/edit#
